var mongo = require('mongodb');
var config = require('../config');

// Retrieve all the products - GET /api/products
var retrieveAll = function() {
    return function (req, res) {
        console.log("Route: GET /api/products");
        // Get the database object
        const db = req.app.locals.db;
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Query Operator
                var query = {};
                var projection = {name: 1, cuisine: 1, restaurant_id: 1, "address.street": 1, borough: 1};
                // Retrieve all products
                // collection.find(query).toArray()
                collection.find(query).project(projection).limit(config.LIMIT).toArray()
                // collection.find(query).sort({'name': -1}).limit(config.LIMIT).toArray()
                .then(function(results) {
                    console.log(results);
                    res.json(results);
                })
                .catch(function(err) {
                    console.log("Error retrieving all: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for retrieve all: " + err);
            res.status(500).send(err);
        }
    }
};

// Retrieve one product using objectID - GET /api/products/:id
var retrieveOne = function() {
    return function (req, res) {
        console.log("Route: GET /api/products/:id");
        console.log("Object ID to retrieve: " + req.params.id);
        // Convert to object ID type
        var param = new mongo.ObjectID(req.params.id);
        // Get the database object
        const db = req.app.locals.db;
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Query Operator
                var query = {"_id": param};
                // var options = {fields: {type: 1, title: 1, description: 1, pricing: 1}}
                var options = {};
                // Retrieve one product
                collection.findOne(query, options)
                .then(function(result) {
                    console.log("Retrieve One: " + result);
                    res.json(result);
                })
                .catch(function(err) {
                    console.log("Error retrieving: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for retriev one: " + err);
            res.status(500).send(err);
        }
    }
};

// Insert one product (Pass the product document using JSON body) - POST /api/products
var insertOne = function() {
    return function (req, res) {
        console.log("Route: POST /api/products");
        // Get the database object
        const db = req.app.locals.db;
        // Insert the new product (product JSON info in req.body)
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Insert new record
                console.log("New Product: ", req.body);
                var doc = JSON.parse(req.body.newProduct); // convert json to object
                var options = {};
                collection.insertOne(doc,options)
                .then(function(result) {
                    console.log("Insert: " + result);
                    res.json(result);
                })
                .catch(function(err) {
                    console.log("Error inserting: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for Insert: " + err);
            res.status(500).send(err);
        }
    }
};

// Update one product using objectID (set parameters in JSON body) - PUT /api/products
var updateOne = function() {
    return function (req, res) {
        console.log("Route: PUT /api/products/:id");
        console.log("Object ID to update: " + req.params.id);
        // Convert to object ID type
        var param = new mongo.ObjectID(req.params.id);
        var doc = JSON.parse(req.body.updateProduct); // convert json to object
        // Get the database object
        const db = req.app.locals.db;
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Filter, Update, Options Operator
                var filter = {"_id": param};
                // var update = {$set: req.body};
                var update = {$set: doc};
                var options = {upsert: false};
                // Update one product
                collection.updateOne(filter,update,options)
                .then(function(result) {
                    console.log(result.result);
                    res.json(result);
                })
                .catch(function(err) {
                    console.log("Error updating: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for update: " + err);
            res.status(500).send(err);
        }
    }
};

// Insert new grade for product using objectID (set parameters in JSON body) - PUT /api/products/grades
var updateGrade = function() {
    return function (req, res) {
        console.log("Route: PUT /api/products/grades/:id");
        console.log("Object ID to update: " + req.params.id);
        // Convert to object ID type
        var param = new mongo.ObjectID(req.params.id);
        var doc = JSON.parse(req.body.newGrades); // convert json to object
        // Get the database object
        const db = req.app.locals.db;
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Filter, Update, Options Operator
                var filter = {"_id": param};
                var update = {$push: {'grades': doc}};
                var options = {upsert: false, returnOriginal: false};
                // Insert new grade for product
                // collection.updateOne(filter,update,options)
                collection.findOneAndUpdate(filter,update,options)
                .then(function(result) {
                    console.log(result);
                    newGrade = result.value.grades[result.value.grades.length - 1];
                    res.json(newGrade);
                })
                .catch(function(err) {
                    console.log("Error updating: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for update: " + err);
            res.status(500).send(err);
        }
    }
};

// Delete one product using objectID - DELETE /api/products/:id
var deleteOne = function() {
    return function (req, res) {
        console.log("Route: DELETE /api/products/:id");
        console.log("Object ID to delete: " + req.params.id);
        // Convert to object ID type
        var param = new mongo.ObjectID(req.params.id);
        // Get the database object
        const db = req.app.locals.db;
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Filter Operator and Options
                var filter = {"_id": param};
                var options = {};
                // Delete one product
                collection.deleteOne(filter,options)
                .then(function(result) {
                    console.log(result.result);
                    res.json(result);
                })
                .catch(function(err) {
                    console.log("Error deleting: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for delete: " + err);
            res.status(500).send(err);
        }
    }
};

// Retrieve list of product types - GET /api/product/types
var retrieveTypes = function() {
    return function (req, res) {
        console.log("Route: GET /api/products/types");
        // Get the database object
        const db = req.app.locals.db;
        try {
            db.collection(config.COLL_MAIN,function(err,collection) {
                // Hanlde collection err
                if (err) {
                    res.status(500).send(err);
                };
                // Field to find distinct value
                var key = "cuisine";
                // Query Operator
                var query = {};
                // Options
                var options = {}; 
                // Retrieve distinct types
                collection.distinct(key,query,options)
                .then(function(results) {
                    console.log(results.sort());
                    res.json(results.sort());
                })
                .catch(function(err) {
                    console.log("Error retrieving distinct types: " + err);
                    res.status(500).send(err);
                })
            })
        } catch (err) {
            console.log("Runtime errors for retrieve distinct types: " + err);
            res.status(500).send(err);
        }
    }
};

// Export route handlers
module.exports = {
    retrieveAll: retrieveAll(),
    retrieveOne: retrieveOne(),
    insertOne: insertOne(),
    updateOne: updateOne(),
    deleteOne: deleteOne(),
    retrieveTypes: retrieveTypes(),
    updateGrade: updateGrade()
};

// Does not work
// module.exports = function() {
//   return {
//     retrieveAll: retrieveAll()
//     // create: create(db)
//   }
// };

